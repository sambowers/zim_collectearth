# zim_collectEarth

Area of change by activity in Zimbabwe 2007 - 2017. Analysis conducted during the SEOSAW data analysis and study on drivers of deforestation and degradation workshop, held at Zimbabwe Forestry Commission Harare, 25th February - 1st March.

This is a preliminary analysis only, use with care!

For detail on methods, see Excel file or R script. For more detail, contact sam.bowers@ed.ac.uk.